const proganyLXX = [
  {
    name: "Adam",
    birthyear: 0,
    successor: "Seth",
    agesuccessorborn: 230,
    yearslivedaftersuccessor: 700,
    totalAge: 930,
    deathyear: 930,
  },
  {
    name: "Seth",
    birthyear: 230,
    successor: "Enosh",
    agesuccessorborn: 205,
    yearslivedaftersuccessor: 707,
    totalAge: 912,
    deathyear: 1142,
  },
  {
    name: "Enosh",
    birthyear: 435,
    successor: "Kenan",
    agesuccessorborn: 190,
    yearslivedaftersuccessor: 715,
    totalAge: 905,
    deathyear: 1340,
  },
  {
    name: "Kenan",
    birthyear: 625,
    successor: "Mahalalel",
    agesuccessorborn: 170,
    yearslivedaftersuccessor: 740,
    totalAge: 910,
    deathyear: 1535,
  },
  {
    name: "Mahalalel",
    birthyear: 795,
    successor: "Jared",
    agesuccessorborn: 165,
    yearslivedaftersuccessor: 730,
    totalAge: 895,
    deathyear: 1690,
  },
  {
    name: "Jared",
    birthyear: 960,
    successor: "Enoch",
    agesuccessorborn: 162,
    yearslivedaftersuccessor: 800,
    totalAge: 962,
    deathyear: 1922,
  },
  {
    name: "Enoch",
    birthyear: 1122,
    successor: "Methuselah",
    agesuccessorborn: 165,
    yearslivedaftersuccessor: 0,
    totalAge: 0,
    deathyear: 0,
  },
];

const containerTimeline = document.querySelector(".timeline");

const timelineMultiplier = 50;

const displayAncestor = function (persons) {
  containerTimeline.innerHTML = "";
  // person.forEach(function (mov, i) {
  //   console.log(i);
  // const html = `
  //   <div class="movements__row">
  //     <div class="movements__type movements__type--${type}">${i + 1}</div>
  //     <div class="movements__date">3 days ago</div>
  //     <div class="movements__value">${mov}€</div>
  //   </div>`;

  //   containerTimeline.insertAdjacentHTML("afterbegin", html);
  // });
  console.log(persons);
  persons.forEach(function (person) {
    console.log(person);
    const html = `
      <div class="person flex" 
        style="
          margin-left:${person.birthyear / timelineMultiplier}rem;
          width:${person.totalAge / timelineMultiplier}rem;
        ">
        ${person.name}
      </div>
    `;
    containerTimeline.insertAdjacentHTML("afterbegin", html);
  });
};
displayAncestor(proganyLXX);

console.log(proganyLXX[0]);

let previousScrollState = window.scrollY;
const headerEl = document.querySelector("header");
const footerEl = document.querySelector("footer");

window.onscroll = function () {
  console.log(window.scrollY);
  if (previousScrollState < window.scrollY) {
    headerEl.classList.add("scrolling");
    footerEl.classList.add("scrolling");
  } else {
    headerEl.classList.remove("scrolling");
    footerEl.classList.remove("scrolling");
  }

  previousScrollState = window.scrollY;
};

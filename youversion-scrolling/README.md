# Youversion scrolling

## Minimal Viable Product

Mobile first.
Recreating the design of the YouVersion Bible app, bible section. and implementing the scrolling features.

## Decronstruction

### Design

1. Top menu
2. Middle section with content
3. Bottom menu + next/previous chapter buttons

### Functionality

Scrolling down
1. Top menu becomes minimal
2. Content scrolls
3. Bottom part slides out of view

Scrolling up
1. Reverse of scrolling down

# All my cats
# catNames = []
# while True:
#     print('Enter the name of cat ' + str(len(catNames) + 1) + ' (Or enter nothing to stop.):')
#     name = input()
#     if name == '':
#         break
#     catNames = catNames + [name]
# print('The cat names are:')
# for name in catNames:
#     print('   ' + name)

# supplies = ['pens', 'staplers', 'flamethrowers', 'binders']
#
# print(range(len(supplies)))
#
# for i in range(len(supplies)):
#     print('Index ' + str(i) + ' in supplies is: ' + supplies[i])

# My pets
# myPets = ['Sophi', 'Chris', 'Jerry']
# print('Enter a pet name:')
# name = input()
# if name not in myPets:
#     print('I do not have a pet named ' + name)
# else:
#     print(name + ' is my pet.')

# Multiple Assignment Trick a.k.a. Tuple unpacking
# cat = ['fat', 'gray', 'loud']
# size, color, disposition = cat

# enumerate() function
# supplies = ['pens', 'staplers', 'flamethrowers', 'binders']
# for index, item in enumerate(supplies):
#     print('Index ' + str(index) + ' in supplies is: ' + item)

# random.choice() and random.shuffle()
# import random
#
# pets = ['Dog', 'Cat', 'Platopus']
#
# print(random.choice(pets))
#
# print(pets)
# random.shuffle(pets)
# print(pets)

# Augmented Assignment
# spam = 32
# print(spam)
# spam = spam + 1
# print(spam)
# spam += 1
# print(spam)
#
# Augmented Assignment Operators
# +=
# -=
# *=
# /=
# %=

# Concatenation and replication
# spam = 'Hello,'
# spam += ' world!'
# print(spam)
#
# name = ['Daniel']
# name *= 3
# print(name)

# Methods

# index()
# greeting = ['Hello', 'Hi', 'Howdy', 'Heeeeey yaaaa']
# greeting.index('Hello')
# print(greeting.index('Howdy'))

# append()
# animals = ['cat', 'dog', 'chicken']
# animals.append('hamster')
# print(animals)

# insert()
# animals.insert(2, 'capybara')
# print(animals)

# remove()
# animals.remove('dog')
# print(animals)
# use del if you know the index of the value
# del animals[0]

# sort()
# numbers = [2, 5, 3.14, 1, -7]
# numbers.sort()
# print(numbers)
#
# animals = ['cat', 'dog', 'elephant', 'cow']
# animals.sort()
# print(animals)
# animals.sort(reverse=True)
# print(animals)
#
# asciiList = ['Alice', 'car', 'Zebra', 'Tzadok', 'Chanok', 'zero']
# asciiList.sort()
# print(asciiList)
#
# asciiList.sort(key=str.lower)
# print(asciiList)

# reverse()
# animals.reverse()
# print(animals)

# 8Ball program with lists
# import random
#
# messages = [
#     'It is certain',
#     'It is decidedly so',
#     'Ask again later'
# ]
#
# print(messages[random.randint(0, len(messages) - 1)])

# Sequence Data Types
# name = 'Tzadok'
# print(name[0])
# print(name[-2])
# print(name[0:4])
#
# if 'Tz' in name:
#     print(True)
# else:
#     print(False)
#
# if 't' in name:
#     print(True)
# else:
#     print(False)
#
# for i in name:
#     print('***' + i + '***')

# Mutable and Immutable Data Types


import time

def collatz(number):
    if (number % 2) == 0: # check if even
        calc = number // 2
        return calc
    elif(number % 2) == 1: # check if odd
        calc = 3 * number + 1
        return calc

print('Enter a number')
try:
    result = collatz(int(input()))
    print(result)
    while result != 1:
        time.sleep(0.1)
        result = collatz(result)
        print(result)

except ValueError:
    print('Please enter a number')


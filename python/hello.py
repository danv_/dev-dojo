# This program says hello and asks for my name.

# print('Hello, world!')
# print('What is your name?')
# myName = input()
# print('It is good to meet you, ' + myName)
# print('The lenght of your name is:')
# print(len(myName))
# print('What is your age?')
# myAge = input()
# print('You will be ' + str(int(myAge) + 1) + ' in a year.')

# print('My name is')
# for i in range(5):
#     print('Jimmy Five Times (' + str(i) + ')')

# total = 0
# for num in range(101):
#     total = total + num
#     print(total)
# print(total)

# print('My name is')
# i = 0
# while i < 5:
#     print('Jimmy Five Times ('+ str(i) + ')') 
#     i = i + 1

# for i in range(0, 10, 2):
#     print(i)

# import random
#
# for i in range(5):
#     print(random.randint(1, 10))

# import sys
#
# while True:
#     print('Type exit to exit.')
#     response = input()
#     if response == 'exit':
#         sys.exit()
#     print('You typed ' + response + '.')

# Guess the Number
# import random, sys
#
# number = random.randint(1, 20)
# attempt = 0
#
# print('I am thinking of a number between 1 and 20')
# print('Take a guess.')
# while True:
#     guess = int(input())
#     if guess == number:
#         print('Good job! You gessed my number in ' + str(attempt) + ' guesses!')
#         sys.exit()
#     elif guess < number:
#         print('Your guess is too low')
#         attempt = attempt + 1
#     elif guess > number:
#         print('Your guess is too high')
#         attempt = attempt + 1

# This is a guess the number game
# import random
# secretNumber = random.randint(1, 20)
# print('I am thinking of a number between 1 and 20.')
#
# # Ask the player to guess 6 times.
# for guessesTaken in range(0, 6):
#     print('Take a guess.')
#     guess = int(input())
#
#     if guess < secretNumber:
#         print('Your guess is too low.')
#     elif guess > secretNumber:
#         print('Your guess is too high.')
#     else:
#         break # This condition is the correct guess!
#     if guess == secretNumber:
#         print('Good job!')
#     else:
#         print("Nope.")

# import random
# spam = random.randint(1, 3)
#
# if spam == 1:
#     print('Hello')
# elif spam == 2:
#     print('Howdy')
# else:
#     print('Greetings')

for i in range(1, 11):
    print(i)
    i = i + 1

n = 1
while n < 11:
    print(n)
    n = n + 1

# def hello():
#     print('Howdy!')
#     print('Howdy!!!')
#     print('Hello there.')
#
# hello()
# hello()

# def hello(name):
#     print('Hello, ' + name)
#
# hello('Alice')
# hello('Bob')

# 8 ball
# import random
#
# def getAnswer(answerNumber):
#     if answerNumber == 1:
#         return 'It is certain'
#     elif answerNumber == 2:
#         return 'It is decidedly so'
#     elif answerNumber == 3:
#         return 'Yes'
#     elif answerNumber == 4:
#         return 'Reply hazy ty again'
#     elif answerNumber == 5:
#         return 'Ask again later'
#     elif answerNumber == 6:
#         return 'Concentrate and ask again'
#     elif answerNumber == 7:
#         return 'My reply is no'
#     elif answerNumber == 8:
#         return 'Outlook not so good'
#     elif answerNumber == 9:
#         return 'Very doubtful'
#
# # r = random.randint(1, 9)
# # question = getAnswer(r)
# # print(question)
#
# print(getAnswer(random.randint(1,9)))

# print("Hello", end="")
# print("World")

# print('cats', 'dogs', 'mice')
# print('cats', 'dogs', 'mice', sep=', ')

# abcdCallStack
# def a():
#     print('a() starts')
#     b()
#     d()
#     print('a() returns')
#
# def b():
#     print('b() starts')
#     c()
#     print('b() returns')
#
# def c():
#     print('c() starts')
#     print('c() returns')
#
# def d():
#     print('d() starts')
#     print('d() returns')
#
# a()

# Local and Global variables with the same name
# def spam():
#     eggs = 'spam local'
#     print(eggs)
#
# def bacon():
#     eggs = 'bacon local'
#     print(eggs)
#     spam()
#     print(eggs)
#
# eggs = 'global'
# bacon()
# print(eggs)

# Global statement
# def spam():
#     global eggs
#     eggs = 'spam'
#
# eggs = 'global'
# spam()
# print(eggs)

# sameNameLocalGlobal
# def spam():
#     global eggs
#     eggs = 'spam' # global
#
# def bacon():
#     eggs = 'bacon' # local
#
# def ham():
#     print(eggs) # global
#
# eggs = 42 # global
# spam()
# print(eggs)

# divide by zero error
def spam(divideBy):
    try:
        return 42/ divideBy
    except ZeroDivisionError:
        print('Error: Invallid argument.')

print(spam(2))
print(spam(12))
print(spam(0))
print(spam(1))
